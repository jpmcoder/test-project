const express = require('express');
const cors = require('cors');
const fs = require("fs");
const parsedJson = (JSON.parse(fs.readFileSync("./demo.json", "utf8")));
const key = require('./config')
const app = express();

// Cors stands for cross-origin resource sharing.
// This is being to allow restricted resources on a web page to be requested ...
// from another domain outside the domain from which the first resource was served.
app.use(cors());
// This is a method inbuilt in express to recognize the incoming Request Object as a JSON Object.
app.use(express.json());

app.use(function(req, res, next) {
    res.header(`Access-Control-Allow-Origin`, `http://localhost:8000`)
    res.header(`Access-Control-Allow-Credentials`, true)
    res.header(
      `Access-Control-Allow-Headers`,
      `Origin, X-Requested-With, Content-Type, Accept`
    )
    next();
});

// @route   GET /api/v1.0/data
// @desc    Get the sample data form the json file
// @access  Public
app.get('/api/v1.0/data', (req, res) => {
    // This is the response
    res.send(parsedJson);
});

// User 5000 port if environment port is empty.
// Environment port value usually appear whe deployed on an actual server.
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));