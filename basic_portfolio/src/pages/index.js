import React, { Component } from "react"
import axios from "axios"
// Layout is a component from components folder
// It consist of the header and footer
import Layout from "../components/layout"
// SEO is a component from components folder
// This is being use so that the developer can rank this website on Search Engines
import SEO from "../components/seo"
import Graph from "../components/graph"

class IndexPage extends Component {

  // Current state of the data for the graph
  state = {
    data: []
  }

  componentDidMount() {
    // Set the state of the data
    this.getSampleData()
  }

  // API call in order to get the graph data from the backend
  getSampleData() {
    axios
    .get('http://localhost:5000/api/v1.0/data')
    .then(res =>
      this.setState({
        data: res.data
      })
    )
    .catch(err => {
      console.log(err)
    });
  }
  
  // Function that shows content that shows if the state data is set
  setupGraphOption() {
    if(this.state.data.length === 0) {
      // If set show Graph is loading
      return <p>Graph Loading...</p>
    } else {
        // If set show Graph component
      return <Graph data={this.state.data.demo[0]} />
    }
  }

  render() {
    return (
      <Layout>
        <SEO title="Home" />
        <h1>Welcome to my Website</h1>
        <p>This is a sample site for the Gatsby basic portfolio.</p>
        {this.setupGraphOption()}
      </Layout>
    )
  }

}

export default IndexPage
