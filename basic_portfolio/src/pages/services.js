import React from 'react'
// Layout is a component from components folder
// It consist of the header and footer
import Layout from "../components/layout"

const ServicesPage = () => {
    return (
        <Layout>
            <h1>Services</h1>
            {/* Lorem Ipsum is a dummy text testing purposes */}
            <p>
                Anim magna ipsum aliquip voluptate ex deserunt reprehenderit pariatur et non. Sunt ad minim ex laboris. In fugiat proident consequat commodo ipsum aliqua Lorem anim quis. Aute commodo voluptate esse sint minim do aute occaecat ullamco duis anim. Laborum consequat excepteur eu do nostrud duis labore commodo amet laborum cillum culpa non. Excepteur adipisicing qui magna excepteur occaecat commodo adipisicing id. Nisi commodo aute nulla culpa consequat sit ut fugiat.
            </p>
        </Layout>
    )
}

export default ServicesPage
