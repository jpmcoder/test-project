import React from "react"
// Link is being use for a component based page linking rather than using a tag
import Link from "gatsby-link"

const Menu = () => {
  return (
  <div
    style={{
      background: "#f4f4f4",
      paddingTop: "10px",
    }}
  >
    <ul
      style={{
        listStyle: "none",
        display: "flex",
        justifyContent: "space-evenly",
      }}
    >
      <li>
        {/* When clicked, you will be redirected to Home page */}
        <Link to="/">Home</Link>
      </li>
      <li>
        {/* When clicked, you will be redirected to About page */}
        <Link to="/about">About</Link>
      </li>
      <li>
        {/* When clicked, you will be redirected to Services page */}
        <Link to="/services">Services</Link>
      </li>
    </ul>
  </div>
  )
}

export default Menu
