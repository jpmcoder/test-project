import React, { Component } from "react"
import CanvasJSReact from "../assets/canvasjs.react"
var CanvasJSChart = CanvasJSReact.CanvasJSChart
 
class Graph extends Component {
  
  // Call the this function to create the graph
  // Argument 1 is the data to create the lines
  // Argument 2 is the the line number to be able to use multi line graph
  createLineGraph = (data, value) => {
    let dataPoints = []
    for (let i = 0; i < Object.keys(data).length; i++) {
      console.log(data[i+1][value])
      if(data[i+1][value] !== '') {
        dataPoints.push({ x: i+1, y: parseInt(data[i+1][value], 10) })
      } else {
        dataPoints.push({ x: i+1, y: parseInt(0, 10) })
      }
    }
    return dataPoints
  }

	render() {
    // This graph data came from the parent component
    const graph = this.props.data
    // This is the options for the graph
		const options = {
      // True will enable all the animation provided by CanvasJS
      // False will disable
      animationEnabled: true,
      title:{
        // The title of the graph
        text: graph.title
      },
      axisX: {
        // The name to vertical line
        title: graph['x-axis']
      },
      // This is an array of dataSeries Objects. dataSeries is explained in detail below.
      data: [{
        // Sets the dataSeries name. dataSeries name is shown in various places like toolTip & legend unless overridden.
        name: graph['series-1'],
        // Sets the type of chart to be rendered for corresponding dataSeries. One can choose from the following options.
        // To know more about other types, visit this link. https://canvasjs.com/docs/charts/chart-options/data/type/
        type: "spline",
        // It represents collection of dataPoints inside dataSeries
        dataPoints: this.createLineGraph(graph.data, 0)
      },
      {
        // Sets the dataSeries name. dataSeries name is shown in various places like toolTip & legend unless overridden.
        name: graph['series-2'],
        // Sets the type of chart to be rendered for corresponding dataSeries. One can choose from the following options.
        // To know more about other types, visit this link. https://canvasjs.com/docs/charts/chart-options/data/type/
        type: "spline",
        dataPoints: this.createLineGraph(graph.data, 1)
      }]
		}
		
		return (
		<div>
			<CanvasJSChart options = {options} 
				/* onRef={ref => this.chart = ref} */
			/>
			{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
		</div>
		);
	}
}

export default Graph;                           